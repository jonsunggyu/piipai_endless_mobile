import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:piiprent/helpers/functions.dart';
import 'package:piiprent/screens/widgets/primary_button.dart';
import 'package:piiprent/models/role_model.dart';
import 'package:piiprent/constants.dart';
import 'package:piiprent/login_provider.dart';
import 'package:piiprent/services/contact_service.dart';
import 'package:piiprent/services/login_service.dart';
import 'package:piiprent/widgets/size_config.dart';
import 'package:provider/provider.dart';

class SwitchAccount extends StatefulWidget {
  const SwitchAccount({Key key}) : super(key: key);

  @override
  State<SwitchAccount> createState() => _SwitchAccountState();
}

class _SwitchAccountState extends State<SwitchAccount> {
  LoginService loginService;
  LoginProvider loginProvider;

  @override
  initState() {
    loginService = Provider.of<LoginService>(context, listen: false);
    loginProvider = Provider.of<LoginProvider>(context, listen: false);

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    List<Role> roles = loginService.user.roles;
    LoginProvider loginProvider =
        Provider.of<LoginProvider>(context, listen: false);

    return PopupMenuButton(
      offset: Offset(MediaQuery.of(context).size.width, 0),
      elevation: 0,
      position: PopupMenuPosition.under,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.all(
          Radius.circular(12.0),
        ),
      ),
      itemBuilder: (context) {
        var data = roles.map((role) {
          return PopupMenuItem(
            value: role.id,
            padding: EdgeInsets.zero,
            child: Container(
              width: Get.width,
              color: (role.active)
                  ? Colors.blue[400].withOpacity(0.15)
                  : Colors.white,
              child: Column(
                children: [
                  ListTile(
                    minLeadingWidth: SizeConfig.widthMultiplier * 14.9,
                    onTap: () async {
                      if (role.active == false) {
                        role.active = true;
                        int index = roles
                            .indexWhere((element) => element.id == role.id);

                        loginProvider.switchRole = index;

                        Navigator.pushNamedAndRemoveUntil(
                          context,
                          '/',
                          (route) => false,
                        );
                      }
                    },
                    title: Container(
                      child: Text(
                        loginService.user.name,
                        textAlign: TextAlign.center,
                        overflow: TextOverflow.visible,
                        maxLines: 1,
                        style: TextStyle(
                          fontSize: SizeConfig.heightMultiplier * 2.50,
                          color: Colors.blue[700],
                          fontWeight: FontWeight.w700,
                        ),
                      ),
                    ),
                    subtitle: Padding(
                      padding: EdgeInsets.only(
                        top: SizeConfig.heightMultiplier * 0.61,
                      ),
                      child: Container(
                        alignment: Alignment.center,
                        width: Get.width / 2.2,
                        child: Text(
                          getRolePosition(role),
                          maxLines: 1,
                          overflow: TextOverflow.ellipsis,
                          style: TextStyle(
                            fontSize: SizeConfig.heightMultiplier * 1.95,
                            color: Colors.grey,
                            fontWeight: FontWeight.w700,
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          );
        }).toList();

        return [
          ...data,
          PopupMenuItem(
            padding: EdgeInsets.zero,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  height: SizeConfig.heightMultiplier * 0.59,
                ),
                Divider(),
                SizedBox(
                  height: SizeConfig.heightMultiplier * 0.59,
                ),
                Align(
                  alignment: Alignment.center,
                  child: Padding(
                    padding: EdgeInsets.all(1),
                    child: PrimaryButton(
                      height: SizeConfig.heightMultiplier * 6,
                      btnText: 'Logout',
                      buttonColor: Colors.blue[700],
                      onPressed: () {
                        loginService.logout(context: context).then(
                              (bool success) =>
                                  Navigator.pushNamed(context, '/'),
                            );
                      },
                    ),
                  ),
                ),
                SizedBox(
                  height: SizeConfig.heightMultiplier * 1.46,
                ),
              ],
            ),
          )
        ];
      },
      child: InkWell(
        child: AccountImage(),
      ),
    );
  }
}

String roleAndName(index, name, loginService) {
  String n = name[index].contains(loginService.user.name)
      ? name[index].replaceAll("${loginService.user.name}", ",")
      : (name[index].split("-").first + ",");
  String c = "${loginService.user.companyName}";
  return n + c;
}

class AccountImage extends StatefulWidget {
  const AccountImage({Key key}) : super(key: key);

  @override
  State<AccountImage> createState() => _AccountImageState();
}

class _AccountImageState extends State<AccountImage> {
  LoginService loginService;

  @override
  initState() {
    loginService = Provider.of<LoginService>(context, listen: false);
    // debugPrint('roles: ============= ${loginService.user.roles}');
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    ContactService contactService = Provider.of<ContactService>(context);
    return FutureBuilder(
      future: contactService.getContactPicture(loginService.user.userId),
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        return Container(
          //margin: EdgeInsets.symmetric(vertical: 15),
          // margin: EdgeInsets.symmetric(
          //   vertical: 12/*SizeConfig.heightMultiplier * 2.34*/,
          // ),
          margin: EdgeInsets.only(top: 3, right: 5),
          height: 28,
          width: 28,
          child: Consumer<LoginProvider>(
            builder: (_, login, __) {
              return Container(
                height: SizeConfig.heightMultiplier * 6.86,
                width: SizeConfig.widthMultiplier * 9.73,
                // height: 40,
                // width: 40,
                clipBehavior: Clip.antiAlias,
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  color: Colors.white,
                ),
                child: Icon(
                  CupertinoIcons.person_fill,
                  size: SizeConfig.heightMultiplier * 4.06,
                  color: primaryColor,
                ),
                // child: login.image == null || login.image == ''
                //     ? Icon(
                //         CupertinoIcons.person_fill,
                //         // size: 90,
                //         size: SizeConfig.heightMultiplier * 4.06,
                //         color: primaryColor,
                //       )
                //     : ClipRRect(
                //         borderRadius: BorderRadius.circular(60),
                //         // borderRadius: BorderRadius.circular(
                //         //   SizeConfig.heightMultiplier *
                //         //       34.5 /
                //         //       SizeConfig.widthMultiplier *
                //         //       40.345,
                //         // ),
                //         child: CachedNetworkImage(
                //           imageUrl: login.image,
                //           fit: BoxFit.cover,
                //           cacheManager: login.cacheManager,
                //           progressIndicatorBuilder: (context, val, progress) {
                //             return Loading();
                //           },
                //           errorWidget: (context, url, error) => Error(),
                //         ),
                //       ),
              );
            },
          ),
        );
      },
    );
  }
}

class Loading extends StatelessWidget {
  const Loading({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      // height: SizeConfig.heightMultiplier * 6.86,
      // width: SizeConfig.widthMultiplier * 9.73,
      height: 40,
      width: 40,
      clipBehavior: Clip.antiAlias,
      decoration: BoxDecoration(
        shape: BoxShape.circle,
        color: Colors.white,
      ),
      child: Center(
        child: Container(
          width: 16,
          height: 16,
          child: CircularProgressIndicator(strokeWidth: 2),
        ),
      ),
    );
  }
}

class Error extends StatelessWidget {
  const Error({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: SizeConfig.heightMultiplier * 6.86,
      width: SizeConfig.widthMultiplier * 9.73,
      // height: 40,
      // width: 40,
      clipBehavior: Clip.antiAlias,
      decoration: BoxDecoration(
        shape: BoxShape.circle,
        color: Colors.white,
      ),
      child: Center(
        child: Icon(
          Icons.error,
          color: Colors.red,
          //size: 90,
          size: SizeConfig.heightMultiplier * 13.17,
        ),
      ),
    );
  }
}
